/*5 вариант, лекция 4 массивы, дз на 30 сентября, 1 задание
Дана последовательность натуральных чисел{ aj }j = 1...n(n <= 10000).Если в последовательности нет чисел с суммой цифр, равной 19, упорядочить последовательность по невозрастанию*/
#include <iostream>
#include <algorithm> // для сортировки массива
using std::cout;
using std::cin;
using std::endl;
int main()
{
	setlocale(LC_ALL, "rus");
	cout << "Сколько чисел будет в последовательности?" << endl;
	int N;
	cin >> N;
	int* lis = new int[N];
	int x;
	for (int i = 0; i < N; i++) {
		cout << "Введите " << i + 1 << " число массива" << endl;
		cin >> x;
		lis[i] = x;

	}
	int numsum = 0;
	int num;
	for (int i = 0; i < N; i++) {
		num = lis[i];
		while (num > 0) {
			numsum += num % 10;
				num /= 10;
		}
		if (numsum == 19) {
			cout << "Есть число, сумма цифр которого равна 19" << endl;
			break;
		}
		else
			numsum = 0;
	}
	for (int i = 0; i < N; i++)
		for (int x = i+1; x < N; x++) {
			if (lis[i] >= lis[x])
				std::swap(lis[i], lis[x]);
		}
	cout << "Отсортированный массив:" << endl;
	for (int i = 0; i < N; i++) {
		cout << lis[i] << ", ";
	}

	

}
