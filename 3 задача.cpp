/*5 вариант, лекция 4 массивы, дз на 30 сентября, 3 задание
Дана целочисленная матрица {Aij}i=1..n,j=1..m (n,m<=100). Найти строку с наибольшей по абсолютной величине суммой элементов и заменить 
все элементы этой строки числом 9999. */
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
	setlocale(LC_ALL, "rus");
	cout << "Сколько будет строк в матрице?" << endl;
	int i;
	cin >> i;
	cout << "Сколько будет столбцов?" << endl;
	int j;
	cin >> j;
	int lis[100][100];
	//ввод матрицы
	for (int x = 0; x < i; x++) {
		cout << "Введите " << x + 1 << " строку" << endl;
		for (int y = 0; y < j; y++) {
			cin >> lis[x][y];
		}
	}
	cout << "матрица" << endl;
	int sumstr; //сумма элементов строки
	int maxsum = INT_MIN; //максимальная сумма элементов строки
	int Imax; // номер строки с максимальной суммой
	//поиск строки с максимальной суммой элементов
	for (int x = 0; x < i; x++) {
		sumstr = 0;
		for (int y = 0; y < j; y++) {
			sumstr += lis[x][y];
		}
		if (sumstr > maxsum) {
			maxsum = sumstr;
			Imax = x; 
		}
	}
	//замена каждого элемента в строке с максимальной суммов на 9999
	for (int y = 0; y < j; y++) {
		lis[Imax][y] = 9999;
	}
	//вывод матрицы
	for (int x = 0; x < i; x++) {
		for (int y = 0; y < j; y++) {
			cout << lis[x][y] << " ";
			
		}
		cout << '\n';
	}
}
